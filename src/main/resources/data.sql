/*****MOVIE*****/
INSERT INTO movie (movie_title, genre, release_year, director, poster_url, trailer_link) VALUES ('The Dark Knight Rises', 'Action, Superhero', 2012, 'Christopher Nolan', 'posterone.org', 'evenbetter.org');
INSERT INTO movie (movie_title, genre, release_year, director, poster_url, trailer_link) VALUES ('Spider-Man: Far from Home', 'Action, Comedy, Superhero', 2019, 'Jon Watts', 'poster.org', 'totsie.com');
INSERT INTO movie (movie_title, genre, release_year, director, poster_url, trailer_link) VALUES ('Black Panther', 'Action, Adventure', 2018, 'Ryan Coogler', 'amazing.org', 'cats.org');
INSERT INTO movie (movie_title, genre, release_year, director, poster_url, trailer_link) VALUES ('Guardians of the Galaxy', 'Action, Comedy, Superhero', 2014, 'James Gunn', 'wowzer.org', 'amazingtrailer.com');

/*****CHARACTER*****/
/*The Dark Knight Rises*/
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Christian Bale', 'Baleale', 'male', 'thisisurl.com');
/*Spider-Man: Far from Home*/
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Zendaya Coleman', 'Zendaya', 'female', 'anotherurl.org');
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Tom Holland', 'Spiderman', 'male', 'meh.com');
/*Black Panther*/
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Chadwick Boseman', 'Boseman', 'male', 'poke.org');
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Michael B Jordan', 'notbasket', 'male', 'notbasket.com');
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Lupita Nyongo', 'Lupita', 'female', 'weewoo.com');
/*Guardians of the Galaxy*/
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Chris Pratt', 'dinosaur', 'male', 'park.com');
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Zoe Saldana', 'Zoe', 'female', 'map.com');
INSERT INTO character (full_name, alias, gender, picture_url) VALUES ('Dave Bautista', 'Bautista', 'male', 'wwe.no');

/*****FRANCHISE*****/
INSERT INTO franchise(name, description) VALUES ('Marvel Cinematic Universe', 'The Marvel Cinematic Universe is an American media franchise and shared universe centered on series of superhero films produced by Marvel Studios.');
INSERT INTO franchise(name, description) VALUES ('Batman', 'Batman franchise description');


INSERT INTO movie_character(movie_id, character_id) VALUES (1,1);
INSERT INTO movie_character(movie_id, character_id) VALUES (2,2);
INSERT INTO movie_character(movie_id, character_id) VALUES (2,3);
INSERT INTO movie_character(movie_id, character_id) VALUES (3,4);
INSERT INTO movie_character(movie_id, character_id) VALUES (3,5);
INSERT INTO movie_character(movie_id, character_id) VALUES (3,6);
INSERT INTO movie_character(movie_id, character_id) VALUES (4,7);
INSERT INTO movie_character(movie_id, character_id) VALUES (4,8);
INSERT INTO movie_character(movie_id, character_id) VALUES (4,9);



