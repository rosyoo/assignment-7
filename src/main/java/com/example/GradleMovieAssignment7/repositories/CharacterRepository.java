package com.example.GradleMovieAssignment7.repositories;

import com.example.GradleMovieAssignment7.models.Character;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {
}
