package com.example.GradleMovieAssignment7.repositories;

import com.example.GradleMovieAssignment7.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Long> {
}
