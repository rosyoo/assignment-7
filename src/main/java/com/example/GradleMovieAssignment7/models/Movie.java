package com.example.GradleMovieAssignment7.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "movie")
public class Movie {
    //Autoincremented Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //Movie Title
    @NotNull
    @Column(name = "movie_title")
    private String movieTitle;

    //Genre (just a simple string of comma separated genres, there is no genre modelling required as a base)
    @Column(name = "genre")
    private String genre;

    //Release year
    @Column(name = "release_year")
    private long releaseYear;

    //Director (just a string name, no director modelling required as a base)
    @Column(name = "director")
    private String director;

    //Picture (URL to a movie poster)
    @Column(name = "poster_url")
    private String posterUrl;

    //Trailer (YouTube link most likely)
    @Column(name = "trailer_link")
    private String trailerLink;

    //Relationships
    @ManyToMany
    @JoinTable(name = "movie_character",
            joinColumns = {@JoinColumn(name="character_id")},
            inverseJoinColumns = {@JoinColumn(name="movie_id")}
    )
    Set<Character> characters;

    @JsonGetter("characters")
    public List<String> charactersGetter(){
        if(characters != null){
            return characters.stream().map(character -> {
                return "/api/v1/characters/" + character.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    @JsonGetter("franchise")
    public String franchiseGetter(){
        if(franchise != null){
            return "/api/v1/franchises/" + franchise.getId();
        }
        return null;
    }

    public Movie(long id, String movieTitle, String genre, long releaseYear, String director, String posterUrl, String trailerLink, Set<Character> characters, Franchise franchise) {
        this.id = id;
        this.movieTitle = movieTitle;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.posterUrl = posterUrl;
        this.trailerLink = trailerLink;
        this.characters = characters;
        this.franchise = franchise;
    }

    public Movie() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public void setMovieTitle(String movieTitle) {
        this.movieTitle = movieTitle;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public long getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(long releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getTrailerLink() {
        return trailerLink;
    }

    public void setTrailerLink(String trailerLink) {
        this.trailerLink = trailerLink;
    }

    public Set<Character> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<Character> characters) {
        this.characters = characters;
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }
}
