package com.example.GradleMovieAssignment7.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "franchise")
public class Franchise {
    //Autoincremented Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //Name
    @NotNull
    @Column(name = "name")
    private String name;

    //Description
    @Column(name = "description")
    private String description;

    //Relationships
    @OneToMany(mappedBy = "franchose")
    Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter(){
        if(movies != null){
            return movies.stream().map(movie -> {
                return "/api/v1/movies/" + movie.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    public Franchise(long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Franchise() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
