package com.example.GradleMovieAssignment7.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.sun.istack.NotNull;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "character")
public class Character {
    //Autoincremented Id
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    //Full name
    @NotNull
    @Column(name = "full_name", nullable = false, length = 100)
    private String fullName;

    //Alias (if applicable)
    @Column(name = "alias", length = 100)
    private String alias;

    //Gender
    @Column(name = "gender")
    private String gender;

    //Picture (URL to photo – do not store an image)
    @Column(name = "picture_url")
    private String pictureUrl;

    //Relationships
    @ManyToMany(mappedBy = "characters")
    Set<Movie> movies;

    @JsonGetter("movies")
    public List<String> moviesGetter(){
        if(movies != null){
            return movies.stream().map(movie -> {
                return "/api/v1/movies/" + movie.getId();
            }).collect(Collectors.toList());
        }
        return null;
    }

    public Character(long id, String fullName, String alias, String gender, String pictureUrl) {
        this.id = id;
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.pictureUrl = pictureUrl;
    }

    public Character() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}
