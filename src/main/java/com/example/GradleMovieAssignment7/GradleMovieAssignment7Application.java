package com.example.GradleMovieAssignment7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GradleMovieAssignment7Application {

	public static void main(String[] args) {
		SpringApplication.run(GradleMovieAssignment7Application.class, args);
	}

}
