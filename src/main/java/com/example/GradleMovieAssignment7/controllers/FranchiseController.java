package com.example.GradleMovieAssignment7.controllers;

import com.example.GradleMovieAssignment7.models.Franchise;
import com.example.GradleMovieAssignment7.models.Movie;
import com.example.GradleMovieAssignment7.repositories.FranchiseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/v1/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    public FranchiseController(FranchiseRepository franchiseRepository){
        this.franchiseRepository = franchiseRepository;
    }

    //Get all franchises from database
    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises(){
        HttpStatus status = HttpStatus.OK;
        List<Franchise> franchises = franchiseRepository.findAll();
        return new ResponseEntity<>(franchises, status);
    }

    //Get franchise by id from database
    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getFranchiseById(@PathVariable Long id){
        Franchise franchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            franchise = franchiseRepository.findById(id).get();
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(franchise, status);
    }

    //Get movies in a franchise form database
    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesInFranchise(@PathVariable Long id){
        Set<Movie> movies = null;
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            movies = franchiseRepository.findById(id).get().getMovies();
        }
        else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movies, status);
    }

    //Create a new franchise to database
    @PostMapping
        public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        HttpStatus status;
        if(franchiseRepository.existsById(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(null, status);
        }else{
            status = HttpStatus.CREATED;
            franchise = franchiseRepository.save(franchise);
            return new ResponseEntity<>(franchise, status);
        }
    }

    //TODO: Get characters in franchise
    //TODO: Delete franchise


}
