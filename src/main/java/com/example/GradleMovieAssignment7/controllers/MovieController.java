package com.example.GradleMovieAssignment7.controllers;

import com.example.GradleMovieAssignment7.models.Character;
import com.example.GradleMovieAssignment7.models.Movie;
import com.example.GradleMovieAssignment7.repositories.CharacterRepository;
import com.example.GradleMovieAssignment7.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/movies")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private CharacterRepository characterRepository;

    public MovieController(MovieRepository movieRepository, CharacterRepository characterRepository){
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    //Get all movies from database
    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies(){
        HttpStatus status = HttpStatus.OK;
        List<Movie> movies = movieRepository.findAll();
        return new ResponseEntity<>(movies, status);
    }

    //Get movie by id from database
    @GetMapping("/{id}")
    public ResponseEntity<Movie> getMovieById(@PathVariable Long id){
        Movie movie = new Movie();
        HttpStatus status;
        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).get();
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(movie, status);
    }

    //Get characters in a movie from database
    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<Character>> getCharactersInMovie(@PathVariable Long id){
        Set<Character> characters = null;
        HttpStatus status;

        if(movieRepository.existsById(id)){
            status = HttpStatus.OK;
            characters = movieRepository.findById(id).get().getCharacters();
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(characters, status);
    }

    //Create a new movie to database
    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        HttpStatus status;
        if(movieRepository.existsById(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(null, status);
        }else{
            status = HttpStatus.CREATED;
            movie = movieRepository.save(movie);
            return new ResponseEntity<>(movie, status);
        }
    }

    //Update a movie in database
    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@PathVariable Long id, @RequestBody Movie movie){
        HttpStatus status;
        if(movieRepository.existsById(movie.getId())){
            status = HttpStatus.OK;
            movie = movieRepository.findById(id).orElseThrow();
            movieRepository.save(movie);
            return new ResponseEntity<>(movie, status);
        }else{
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(status);
        }
    }

    //TODO: Update character in movie
    //TODO: Delete movie



}
