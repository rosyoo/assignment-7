package com.example.GradleMovieAssignment7.controllers;

import com.example.GradleMovieAssignment7.models.Character;
import com.example.GradleMovieAssignment7.repositories.CharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/characters")
public class CharacterController {

    @Autowired
    private CharacterRepository characterRepository;

    public CharacterController(CharacterRepository characterRepository){
        this.characterRepository = characterRepository;
    }

    //Get all characters from database
    @GetMapping
    public ResponseEntity<List<Character>> getAllCharacters(){
        HttpStatus status = HttpStatus.OK;
        List<Character> characters = characterRepository.findAll();
        return new ResponseEntity<>(characters, status);
    }

    //Get character by id from database
    @GetMapping("/{id}")
    public ResponseEntity<Character> getCharacterById(@PathVariable Long id){
        Character character = new Character();
        HttpStatus status;

        if(characterRepository.existsById(id)){
            status = HttpStatus.OK;
            character = characterRepository.findById(id).get();
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(character, status);
    }

    //Create a new character to database
    @PostMapping
    public ResponseEntity<Character> addCharacter(@RequestBody Character character){
        HttpStatus status;
        if(characterRepository.existsById(character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(null, status);
        }
        else{
            character = characterRepository.save(character);
            status = HttpStatus.CREATED;
            return new ResponseEntity<>(character, status);
        }
    }

    //Update character in database
    @PutMapping("/{id}")
    public ResponseEntity<Character> updateCharacter(@PathVariable Long id, @RequestBody Character character){
        HttpStatus status;
        if(characterRepository.findById(id).isPresent()){
            Character updatedCharacter = characterRepository.getById(id);
            updatedCharacter.setFullName(character.getFullName());
            updatedCharacter.setAlias(character.getAlias());
            updatedCharacter.setGender(character.getGender());
            updatedCharacter.setPictureUrl(character.getPictureUrl());
            status = HttpStatus.OK;
            character = characterRepository.save(updatedCharacter);
            return new ResponseEntity<>(character, status);
        }else{
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }

    //TODO: Delete character
}
